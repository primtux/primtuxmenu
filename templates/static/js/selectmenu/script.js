"use strict";

const C_SELECTMENU_OPEN_CLASS = 'vertical-menu__container--open';

function open_search(el) {
    if (SEARCHMENU_FOCUS_EL.id == document.activeElement.id)
        return;
    let search_menu = document.getElementById('c-search-menu');
    show_hide(
        search_menu,
        false,
        C_SELECTMENU_OPEN_CLASS
    );
    search_menu.focus();
}

function open_select_menu(_id) {
    let select_menu = document.getElementById('c-select-menu-' + _id);
    show_hide(
        select_menu,
        select_menu.classList.contains(C_SELECTMENU_OPEN_CLASS),
        C_SELECTMENU_OPEN_CLASS
    );
    select_menu.focus();
}

function close_search() {
    let search_menu = document.getElementById('c-search-menu');
    show_hide(
        search_menu,
        true,
        C_SELECTMENU_OPEN_CLASS
    );
    let focus_menu_el = document.getElementById('focus-menu');
    if (focus_menu_el)
        focus_menu_el.focus();
}

function close_select_menu(_id) {
    let select_menu = document.getElementById('c-select-menu-' + _id);
    show_hide(
        select_menu,
        true,
        C_SELECTMENU_OPEN_CLASS
    );
    select_menu.blur();
}

function c_menu_go_level(
    page_index,
    level_name,
    sub_level_name,
    session_name
) {
    go_level(
        page_index,
        level_name,
        sub_level_name,
        session_name
    );
    close_search();
}

function select_menu(
    _id,
    page_index,
    level_name,
    sub_level_name
) {
    let breadcrum_level_el = document.getElementById('c-select-menu-breadcrum-level-' + _id);
    let breadcrum_sub_level_el = document.getElementById('c-select-menu-breadcrum-sub-level-' + _id);

    let field_level_el = document.getElementById(_id + '-level-name');
    let field_sub_level_el = document.getElementById(_id + '-sub-level-name');

    breadcrum_level_el.innerText = level_name;
    field_level_el.value = level_name;
    if (level_name)
        show_hide(breadcrum_level_el, true);

    breadcrum_sub_level_el.innerText = sub_level_name;
    field_sub_level_el.value = sub_level_name;
    if (sub_level_name)
        show_hide(breadcrum_sub_level_el, true);

    close_select_menu(_id);
}
