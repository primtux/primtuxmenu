CREATE TABLE IF NOT EXISTS apps (
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    name     VARCHAR(255) NOT NULL,  /* nom visible par l'utilisateur */
    apt_name VARCHAR(255) NULL,  /* désignation dans apt */

    origin_desktop_name VARCHAR(255) NOT NULL,  /* desktop renseigné dans les sources */
    desktop_path      VARCHAR(2048) NULL, /* chemin du fichier .desktop */

    jerry_icon_path    VARCHAR(2048) NULL, /* chemin de l'icône pour la session Jerry */
    koda_icon_path   VARCHAR(2048) NULL,   /* chemin de l'icône pour la session Koda */
    leon_icon_path    VARCHAR(2048) NULL,  /* chemin de l'icône pour la session Léon */
    poe_icon_path    VARCHAR(2048) NULL,   /* chemin de l'icône pour la session Poe */
    default_icon_path VARCHAR(2048) NULL,  /* chemin de l'icône par défaut (le même pour l'ensemble des sessions) */

    jerry_path    VARCHAR(2048) NULL,      /* chemin de lancement du logiciel pour la session Jerry. ex: /usr/bin/monsoft */
    koda_path   VARCHAR(2048) NULL,        /* chemin de lancement du logiciel pour la session Koda. ex: /usr/bin/monsoft */
    leon_path    VARCHAR(2048) NULL,       /* chemin de lancement du logiciel pour la session Léon. ex: /usr/bin/monsoft */
    poe_path    VARCHAR(2048) NULL,        /* chemin de lancement du logiciel pour la session Poe. ex: /usr/bin/monsoft */
    default_path VARCHAR(2048) NULL,       /* chemin de lancement du logiciel par défaut (le même pour l'ensemble des sessions). ex: /usr/bin/monsoft */

    generic  VARCHAR(255) NULL,  /* infos complémentaires */

    in_jerry  BOOLEAN NOT NULL CHECK (in_jerry IN (0, 1)) DEFAULT 0,  /* présent ou non dans la session Jerry */
    in_koda   BOOLEAN NOT NULL CHECK (in_koda IN (0, 1)) DEFAULT 0,   /* présent ou non dans la session Koda */
    in_leon   BOOLEAN NOT NULL CHECK (in_leon IN (0, 1)) DEFAULT 0,   /* présent ou non dans la session Léon */
    in_poe    BOOLEAN NOT NULL CHECK (in_poe IN (0, 1)) DEFAULT 0,    /* présent ou non dans la session Poe */

    license VARCHAR(255) NULL, /* licence (GPL, BSD ...) */

    is_available BOOLEAN NOT NULL CHECK (is_available IN (0, 1)) DEFAULT 0, /* détermine si le soft est accessible */
    is_apt_installed BOOLEAN NOT NULL CHECK (is_apt_installed IN (0, 1)) DEFAULT 0, /* détermine si le soft est installé dans apt ou non */

    is_web_app BOOLEAN NOT NULL CHECK (is_web_app IN (0, 1)) DEFAULT 0, /* détermine si le soft est une application web ou non */
    is_external_url BOOLEAN NOT NULL CHECK (is_external_url IN (0, 1)) DEFAULT 0, /* détermine si le soft est une application web externe (online) */

    UNIQUE(name) /* force le nom à être unique */

    UNIQUE(origin_desktop_name) /* le desktop dans les sources est unique */

    UNIQUE(jerry_icon_path)    /* force le chemin de l'icône pour la session Jerry à être unique */
    UNIQUE(koda_icon_path)     /* force le chemin de l'icône pour la session Koda à être unique */
    UNIQUE(leon_icon_path)     /* force le chemin de l'icône pour la session Léon à être unique */
    UNIQUE(poe_icon_path)      /* force le chemin de l'icône pour la session Poe à être unique */
    UNIQUE(default_icon_path)  /* force le chemin de l'icône par défaut à être unique */

    UNIQUE(jerry_path)    /* force le chemin de lancement du logiciel pour la session Jerry à être unique */
    UNIQUE(koda_path)     /* force le chemin de lancement du logiciel pour la session Koda à être unique */
    UNIQUE(leon_path)     /* force le chemin de lancement du logiciel pour la session Léon à être unique */
    UNIQUE(poe_path)      /* force le chemin de lancement du logiciel pour la session Poe à être unique */
    UNIQUE(default_path)  /* force le chemin de lancement du logiciel par défaut à être unique */
);

CREATE TABLE IF NOT EXISTS apps_detail (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    app_key     INTEGER,            /* clé étrangère de la table `apps` */
    description TEXT NULL,          /* description de l'application : formatage en html */
    link        VARCHAR(2048) NULL, /* lien vers le site de l'application */

    FOREIGN KEY(app_key) REFERENCES apps(id)
);

CREATE TABLE IF NOT EXISTS levels (
    id                    INTEGER PRIMARY KEY AUTOINCREMENT,
    session               VARCHAR(10) NOT NULL,
    name_level_one        VARCHAR(255) NOT NULL,
    name_level_two        VARCHAR(255) NULL,
    color                 VARCHAR(255) NOT NULL,
    icon_path             VARCHAR(2048) NULL,
    nb_installed_apps     INTEGER,
    nb_not_installed_apps INTEGER,

    UNIQUE(name_level_one, name_level_two, session)
);

CREATE TABLE IF NOT EXISTS app_in_level (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    app_key     INTEGER,            /* clé étrangère de la table `apps` */
    level_key   INTEGER,            /* clé étrangère de la table `levels` */

    FOREIGN KEY(app_key) REFERENCES apps(id),
    FOREIGN KEY(level_key) REFERENCES levels(id)
);

CREATE TABLE IF NOT EXISTS keywords (
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    text VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS contains (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    id_apps     INTEGER,
    id_keywords INTEGER,

    FOREIGN KEY(id_apps) REFERENCES apps(id)
    FOREIGN KEY(id_keywords) REFERENCES keywords(id)
);

CREATE TABLE IF NOT EXISTS preferences (
    id               INTEGER PRIMARY KEY AUTOINCREMENT,
    session_selected VARCHAR(10) NULL,   /* la session sélectionnée dans l'interface d'admin */

    level_name         VARCHAR(255) NULL,  /* précise le niveau sélectionné dans l'interface d'admin */
    sub_level_name     VARCHAR(255) NULL,  /* précise le sous-niveau sélectionné dans l'interface d'admin */

    filter_by        INTEGER, /* détermine si on filtre par "installées" (1), "non installées" (2) ou "tous" (0) dan l'interface d'admin */
    edition          BOOLEAN NOT NULL CHECK (edition IN (0, 1)) DEFAULT 0 /* détermine si on est mode lecture (0) ou édition (1) dans l'interface d'admin */
);

CREATE TABLE IF NOT EXISTS waiting_list (
    id        INTEGER PRIMARY KEY AUTOINCREMENT,
    app_key   INTEGER,                                             /* clé étrangère */
    action    BOOLEAN NOT NULL CHECK (action IN (0, 1)) DEFAULT 0, /* une application dans la liste des tâches est forcément à installer ou à désinstaller */
    state     INTEGER                                              /* 0 : en attente, 1 : en cours de traitement, 2 : en erreur, 3, finalisé */
);
