# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased : 2.0]

### Added

- remplacer flake8 par Ruff : https://github.com/astral-sh/ruff : pas avant d'avoir E303 https://github.com/astral-sh/ruff/pull/8720
- faire le l'appli un PWA : voir sqoosh.app
- mise en place de SEO
- mise en place de thème CSS (personnalisation de son espace)
- indexation ScoLOMFR : https://www.reseau-canope.fr/scolomfr/faq.html
- possibilité de mettre à jour le primtuxmenu.
- proxy avec historisation des pages (contraindre avec des règles les diffs)
- suppression de certaines infos sur les sites/contenus externes
- outil de recherce avec des images de suggestion
- possibilité de coupler les idées de https://framagit.org/mothsart/jurassic-primtux-conf
    - utilisation de **cpulimit**, **cputools**, **ionice** etc pour limiter la charge cpu et IO.

## [Unreleased : 1.0]

### Added

- boutons pré-sélectionnés : ex, [Oui] lors de la demande de suppression
- styler les <input type="radio">
- styler les <select>
- styler le sélecteur de fichiers
- On ne devrait pouvoir créer ou modifier que quand une donnée à réellement été modifé (bouton disable)
- si il y a eu des saisies : poser la question si l'utilisateur souhaite annuler son action.
- les changements de pages devraient changer l'url et on devrait pouvoir piloter la pagination via l'url
- gestionnaire de mots clés (avec auto-complétion sur ceux déjà existants)
- vérifier les md5 des images pour éviter les doublons
- le nom de paquet est différent selon si l'on est sur Debian ou Ubuntu (firefox-esr ou firefox)
- preload images :
    - gestion des erreurs quand le fichier est trop volumineux
    - gestion des erreurs quand le fichier n'est pas interprétable
    - barre de progression

- preload image : meilleur gestion du resize : bilinear interpolation ou Bicubic (plus long mais meilleur) :
    - https://stackoverflow.com/questions/961913/image-resize-before-upload/8397789
    - https://github.com/hMatoba/MinifyJpegAsync

- intégration de Nix

- intégration de gSpeech
- intégration d'un chat bot
- mise en place d'intégration continue
- harmonisation avec https://framagit.org/mothsart/components-mould
- suivre la RFC : https://framagit.org/mothsart/rfc-primtux-web-app
- a11y
- commandes admin en cli
- moteur d'aide interactif
- outil de statistiques d'utilisation
- comportement en mode "réseau"
- gestion des fautes d'orthographes dans la recherche

- ressources :
    - https://www.staedtler.com/fr/fr/mandala-creator
    - https://twitter.com/laclasse_demma/status/1277141893416210437/photo/1
    - http://www2.ac-lyon.fr/services/rhone/maitrise-de-langue/spip.php?article305&lang=fr
    - https://laclassedemallory.net/2020/06/24/porte-cle-memo-et-aide-en-maths-et-francais/
- utiliser les notifications pour signaler (uniquement au prof) qu'une mise à jour est disponible.

## [Unreleased : 0.3]

### Added

- menu : - pouvoir choisir "toutes les applications"
- animation sur un sous-menu quand on change de menu
- upload : mettre à jour le menu lors d'une édition/création/suppression
- message d'erreur si le serveur websocket est down (et éventuellement, bouton de relance)
- [en cours] installation multiple des applications avec barre de progression
- ajout/modification/suppression d'un thème
- outil d'administration entièrement disponible en cli
- utilisation de docker en mode dev
- aide interactive avec une liste d'imprim écran pour présenter le logiciel (sans installation)
- i18n
- mise en place d'un vrai "moteur de recherche"
- ajout d'une entrée de makefile pour créer un commit de changement de db
- créer un système de migration de la base de donnée :
    si detection de la bdd, alors on regarde la version du soft et on fait une migration en conséquence.
- ajout des descriptifs (cf https://primtux.fr/Documentation/site/logiciels-PrimTux-V5.ods)

## [Unreleased : 0.2]

### Added

- Intégration de la nouvelle charte graphique :
    - messages d'erreurs
    - ajout/modification/suppression d'application
    - compter le nb d'applications par menu
    - griser un menu qui n'a pas d'applications ?

- versionning de l'application et de la base de donnée

### Fixed
    - maj diagram SQL
    - "GET /static/js/lib/socket.io.js.map HTTP/1.1" 404 -


## [Unreleased : 0.1]

### Added
- Mise en place d'une vrai interface d'admin
- lancer les apps "web" en <iframe>
- daemon : maj auto de l'interface lors d'ajout/suppression/edition d'applications (Via synaptic, apt etc.)
- flake8 sur l'ensemble du code python
- utilisation de cypress

### Fixed

- faire apparaitre le nombres d'applications disponibles "filtré" par thème
- le filtrage par application ne garde pas en mémoire les thèmes choisis et ne filtre pas en conséquence
- menu : le clic sur "toutes les applications" ne devrait pas faire clignoter le menu => utiliser un masque
- affichage d'un "faux" icone sur les apps qui n'ont pas encore chargé leurs icones.
- mise en place de tests unitaires :
    - chaque icon en bdd a une correspondance dans les assets
    - les icônes sont uniques

## [0.x]

### Added

- création paquet debian et processus de génération
- service systemd + nginx : process de crash et de logs
- menu proche de https://classe-numerique.fr/recherche/
    - accès à toutes les applications une fois qu'une rubrique a été sélectionné
    - faire apparaitre le nombres d'applications disponibles par thème
- filtrage par session et par applications : installés/non installés/tous
- integration de l'ensemble des applications
- permettre la suppression des logiciels (apt)
- menu dynamique
- pagination
- serveur Flask
- HTML orienté composants
- CSS au format BEM
- colorer les requètes SQL
- permettre l'installation des logiciels (apt)

### Fixed

- soucis d'accès à certaines rubriques lié au onblur sur le bouton
