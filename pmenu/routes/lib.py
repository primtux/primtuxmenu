import math

from flask import render_template, request

from pmenu.init import Init
from pmenu.pagination import pagination

init = Init()


def give_levels(request, levels):
    level_name = None
    sub_level_name = None
    if 'level_name' in request.args:
        level_name = request.args['level_name']
    if 'sub_level_name' in request.args:
        sub_level_name = request.args['sub_level_name']

    level_name_exist = False
    sub_level_name_exist = False
    for level in levels:
        if (level['name'] != level_name):
            continue
        if level['nb_apps'] == 0:
            break
        level_name_exist = True
        for sub_level in level['menu']:
            if (sub_level['name'] != sub_level_name):
                continue
            if sub_level['nb_apps'] == 0:
                break
            sub_level_name_exist = True

    if not level_name_exist:
        level_name = None
    if not sub_level_name_exist:
        sub_level_name = None
    return level_name, sub_level_name


async def _update_apps(session_name, session_selected, filter_by):
    init.db_prefs.update({
        'session_selected': session_selected,
        'filter_by': filter_by
    })

    page_index = 0
    level_name = ''
    sub_level_name = ''
    if 'page_index' in request.args and request.args['page_index'].isnumeric():
        page_index = int(request.args['page_index'])
    if 'level_name' in request.args:
        level_name = request.args['level_name']
    if 'sub_level_name' in request.args:
        sub_level_name = request.args['sub_level_name']

    nb_apps = init.db.count_apps_from_level(
        session_selected,
        filter_by,
        level_name,
        sub_level_name
    )
    if nb_apps == 0:
        return {
            'apps': render_template(
                'apps.html',
                nb_apps=0,
                nb_pages=0
            ),
            'pagination': ''
        }
    nb_pages = math.ceil(nb_apps / init.conf.results_by_page)
    apps = init.db.get_page_from_level(
        page_index * init.conf.results_by_page,
        init.conf.results_by_page,
        session_name,
        session_selected,
        filter_by,
        level_name,
        sub_level_name
    ).fetchall()
    session = init.sessions[session_selected]
    apps = render_template(
        'apps.html',
        session=session,
        session_name=session_name,
        apps=apps,
        nb_apps=nb_apps,
        nb_pages=nb_pages
    )
    pagination_t = render_template(
        'pagination.html',
        page_index=page_index,
        session=session,
        session_name=session_name,
        level_name=level_name,
        sub_level_name=sub_level_name,
        pagination=pagination(page_index, nb_pages)
    )
    return {
        'apps': apps,
        'pagination': pagination_t
    }
