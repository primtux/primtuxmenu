from pmenu.requests.apps import DbApps
from pmenu.desktop.insert import Menu, Menus
from pmenu.package import all_apt_installed, desktops_path_for_app


def sync_to_apt(db):
    db_apps = DbApps(db)
    sql_menus = Menus(db.get_levels())

    all_apps = db.get_all_apps_with_levels()

    debs_installed = all_apt_installed()

    for app in all_apps:
        menu = Menu(
            app['session'],
            app['name_level_one'],
            app['name_level_two']
        )
        selected_menu = sql_menus.get(hash(menu))
        selected_menu.list_of_apps_hashes.add(app['name'])

        if app['apt_name'] not in debs_installed:
            db_apps.update({
                'is_available': 0,
                'is_apt_installed': 0,
                'id': app['id']
            })
            if app['name_level_two'] is not None:
                parent_menu = Menu(
                    app['session'],
                    app['name_level_one']
                )
                parent_selected_menu = sql_menus.get(hash(parent_menu))
                parent_selected_menu.list_of_apps_hashes.add(app['name'])
            continue

        if app['name_level_two'] is not None:
            parent_menu = Menu(
                app['session'],
                app['name_level_one']
            )
            parent_selected_menu = sql_menus.get(hash(parent_menu))
            parent_selected_menu.list_of_apps_hashes.add(app['name'])
            parent_selected_menu.list_of_apps_hashes_installed.add(app['name'])
        selected_menu.list_of_apps_hashes_installed.add(app['name'])

        list_of_desktop_files = desktops_path_for_app(app['apt_name'])
        db_apps.update({
            'is_available': 1,
            'is_apt_installed': 1,
            'desktop_path': list_of_desktop_files[0],
            'id': app['id']
        })

    for m in sql_menus:
        len_apps = len(m.list_of_apps_hashes)
        len_installed_apps = len(m.list_of_apps_hashes_installed)
        nb_not_installed_apps = len_apps - len_installed_apps
        if (
            len(m.list_of_apps_hashes_installed) == 0
            and nb_not_installed_apps == 0
        ):
            continue

        db.update_level(
            m.sql_id,
            len(m.list_of_apps_hashes_installed),
            nb_not_installed_apps
        )
    db.db.commit()
