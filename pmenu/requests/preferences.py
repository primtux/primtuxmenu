
class DbPreferences:
    table = 'preferences'

    def __init__(self, db):
        self.db = db

    def get(self):
        return self.db.exec(
            '''
            SELECT * FROM %s WHERE id = 0;
            ''' % self.table
        ).fetchone()

    def update(self, _list):
        _list['id'] = 0
        return self.db.insert_or_update(self.table, _list)
