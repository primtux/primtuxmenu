import unittest

from pmenu.conf import Conf
from pmenu.request import DataBase
from pmenu.desktop.collect import CollectDesktops
from pmenu.desktop.insert import Menus


def is_in_sql(m, sql_menus):
    for _level in sql_menus:
        if hash(m) == hash(_level):
            return True
    return False


class LevelsTest(unittest.TestCase):

    def test_is_levels_in_database(self):
        conf = Conf()

        desktops = CollectDesktops(conf.DEBUG)
        insert_desktops = desktops.collect()

        menus = Menus()
        for desktop_file in insert_desktops.desktop_files:
            menus.populate(desktop_file.jerry_session)
            menus.populate(desktop_file.koda_session)
            menus.populate(desktop_file.leon_session)
            menus.populate(desktop_file.poe_session)

        db = DataBase(conf)

        sql_menus = Menus(db.get_levels())

        for m in menus:
            in_sql = is_in_sql(m, sql_menus)
            if not in_sql:
                print(str(m))
            self.assertTrue(in_sql)
