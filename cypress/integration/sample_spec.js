function screen(_session, suffix='', callback=null) {
  cy.viewport(1280, 768)
  let url = 'http://127.0.0.1:5500/' + _session
  cy.visit(url)
  if (callback)
    callback()
  cy.screenshot({
    name: 'session_' + _session + suffix, 
    clip: { x: 0, y: 0, width: 1280, height: 768 } 
  })
}

function open_menu(_session) {
    screen(
        _session,
        '_open_menu',
        () => {
            cy.get('#select-menu__button').click()
            cy.get('body').focused()
        }
    )
}

describe('Screenshots', () => {

  it('session Jerry', () => {
    screen('jerry')
  })

  it('session Koda', () => {
    screen('koda')
  })

  it('session Léon', () => {
    screen('leon')
  })

  it('session poe', () => {
    screen('poe')
  })

  it("session Poe : ouverture du menu", () => {
    open_menu('poe')
  })

  it("session Poe : ouverture de la description d'une app", () => {
    screen(
      'poe',
      '_open_app_description',
      () => {
        cy.get('li.app__element:nth-child(3)').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
  })

  it("session Poe : ouverture de création d'une app", () => {
    screen(
      'poe',
      '_open_app_creator',
      () => {
        cy.get('#launch-app-creation').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
  })

  it("session Poe : ouverture de la modification d'une app", () => {
    screen(
      'poe',
      '_open_app_update',
      () => {
        cy.get('#edit-mode-checkbox').click()
        cy.get('li.app__element:nth-child(3)').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
    cy.get('#edit-mode-checkbox').click()
  })


  it("session Poe : sidebar d'installation", () => {
    screen(
      'poe',
      '_installation_sidebar',
      () => { 
        cy.get('#installation-preview').click()
      }
    )
  })
})
