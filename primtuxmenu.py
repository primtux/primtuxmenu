import secrets

from flask import Flask, abort, render_template, request

from pmenu.init import UUIdGen
from pmenu.routes.lib import init, _update_apps
from pmenu.routes.index import index_page
from pmenu.routes.menu import menu_page
from pmenu.routes.admin import admin_page
from pmenu.package import apt_install, apt_exist, apt_installed
from pmenu.conf import Conf

app = Flask(
    __name__,
    template_folder='templates'
)

secret = secrets.token_urlsafe(32)
app.secret_key = secret
app.register_blueprint(index_page)
app.register_blueprint(menu_page)
app.register_blueprint(admin_page)


def str_to_bool(value):
    value = value.lower()
    if value == 'true':
        return True
    return False


@app.route('/apps', methods=['GET'])
async def _apps():
    if 'session_name' not in request.args:
        abort(404)
    session_selected = request.args['session_name']
    if 'session_selected' in request.args:
        session_selected = request.args['session_selected']
    filter_by = 0
    if 'filter_by' in request.args:
        filter_by = int(request.args['filter_by'])
    return await _update_apps(
        request.args['session_name'],
        session_selected,
        filter_by
    )


@app.route('/app/<_id>')
async def _app(_id):
    session_name = request.args['session']
    edit_mode = False
    if 'edit_mode' in request.args:
        edit_mode = str_to_bool(request.args['edit_mode'])
    session = init.sessions[session_name]
    app = init.db.get(_id, session_name)
    template_name = 'app_details.html'

    if not edit_mode:
        modal_deco = render_template(
            'decoration_app.html',
            app=app
        )
        modal_body = render_template(
            template_name,
            session=session,
            app=app,
            in_depot=apt_exist(app['apt_name']),
            is_available=apt_installed(app['apt_name']),
            licenses=init.conf.licenses,
            update=True
        )
        modal_footer = render_template(
            'action_app.html',
            session=session,
            app=app,
            in_depot=apt_exist(app['apt_name']),
            is_available=apt_installed(app['apt_name']),
        )
        return {
            'modal_deco': modal_deco,
            'modal_body': modal_body,
            'modal_footer': modal_footer
        }

    modal_deco = render_template(
        'edition/decoration_app.html',
        update=True
    )
    modal_body = render_template(
        'edition/edit_app.html',
        uuid=UUIdGen(),
        session=session,
        menus=init.flask_session['sessions'],
        app=app,
        in_depot=apt_exist(app['apt_name']),
        is_available=apt_installed(app['apt_name']),
        licenses=init.conf.licenses,
        update=True
    )
    modal_footer = render_template(
        'edition/action_app.html',
        session_name=session_name,
        app_id=app['id'],
        app_key=app['app_key'] if 'app_key' in app else None,
        update=True
    )
    return {
        'modal_deco': modal_deco,
        'modal_body': modal_body,
        'modal_footer': modal_footer
    }


@app.route('/launch/<_id>')
async def launch_app(_id):
    session_name = request.args['session']
    app = init.db.get(_id, session_name)
    path = app['path']
    if not path:
        path = app['default_path']

    args = path.split(' ')
    last_arg = args[len(args) - 1]
    if last_arg in ["%f", "%F", "%u", "%U", "%k"]:
        args = args[:-1]

    conf = Conf()
    if conf.last_app != ' '.join(args):
        conf.nb_launched = 0
    conf.last_app = ' '.join(args)
    conf.nb_launched += 1
    conf.update()
    return 'launched'


@app.route('/run/<name>')
async def run_app(name):
    return render_template(
        'run.html'
    )


@app.route('/install/<_id>')
async def install_app(_id):
    app = init.db.get(_id)
    apt_install(app['app_name'])
    return 'installed'
